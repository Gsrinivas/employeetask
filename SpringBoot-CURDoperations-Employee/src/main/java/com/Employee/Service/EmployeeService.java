package com.Employee.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Employee.Model.Employee;
import com.Employee.Repository.EmployeeRepository;


@Service
public class EmployeeService {

	
	@Autowired
	EmployeeRepository employeeRepository;
	
	public List<Employee> getAllEmployee() 
	{
	List<Employee> employee = new ArrayList<Employee>();
	employeeRepository.findAll().forEach(employee1 -> employee.add(employee1));
	return employee;
	}
	
	public Employee getEmployeeById(int Id) 
	{
	return employeeRepository.findById(Id).get();
	}
	
	public void saveOrUpdate(Employee employee) 
	{
	employeeRepository.save(employee);
	}
	
	public void delete(int Id) 
	{
	employeeRepository.deleteById(Id);
	}
	
	public void update(Employee employee, int Id) 
	{
    employeeRepository.save(employee);
	}
	}


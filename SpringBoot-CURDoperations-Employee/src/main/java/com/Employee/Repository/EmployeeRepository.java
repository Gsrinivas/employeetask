package com.Employee.Repository;

import org.springframework.data.repository.CrudRepository;

import com.Employee.Model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}

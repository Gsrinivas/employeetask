package com.Employee.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.Employee.Model.Employee;
import com.Employee.Service.EmployeeService;


@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
    
	
	
	@GetMapping("/emp")
	private List<Employee> getAllEmployee() 
	{
	return employeeService.getAllEmployee();
	}
	
	@GetMapping("/emp/{Id}")
	private Employee getEmployee(@PathVariable("Id") int Id) 
	{
	return employeeService.getEmployeeById(Id);
	}
	
	@DeleteMapping("/emp/{Id}")
	private void delete(@PathVariable("Id") int Id) 
	{
	employeeService.delete(Id);
	}
	
	@PostMapping("/emps")
	private int saveEmployee(@RequestBody Employee employee) 
	{
	employeeService.saveOrUpdate(employee);
	return employee.getEmployeeId();
	}
	
	@PutMapping("/emps")
	private Employee update(@RequestBody Employee employee) 
	{
	employeeService.saveOrUpdate(employee);
	return employee;
	}
	}


